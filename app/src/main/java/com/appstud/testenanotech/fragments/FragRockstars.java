package com.appstud.testenanotech.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.appstud.testenanotech.R;
import com.appstud.testenanotech.adapters.AdaptRockstars;
import com.appstud.testenanotech.entities.MyRockstar;
import com.appstud.testenanotech.tools.JsonParser;
import com.appstud.testenanotech.tools.Tools;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class FragRockstars extends Fragment {
    private View view;
    private EditText etSearch;
    private ListView lvRockstars;
    private List<MyRockstar> data;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.frag_rockstars, container, false);
            loadControls();
            setListeners();
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadControls() {
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        etSearch = (EditText) view.findViewById(R.id.etSearch);
        lvRockstars = (ListView) view.findViewById(R.id.lvRockstars);
    }

    private void setListeners() {
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Tools.hideKeyboard(getActivity());
                    new SearchRockStarsList().execute(etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });

        etSearch.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etSearch.getRight() - etSearch.getCompoundDrawables()[2].getBounds().width())) {
                        Tools.hideKeyboard(getActivity());
                        new SearchRockStarsList().execute(etSearch.getText().toString());
                        return true;
                    }
                }
                return false;
            }
        });

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                new SearchRockStarsList().execute(etSearch.getText().toString());
            }
        });
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (etSearch.getText().toString().equals(""))
                    new RequestRockStarsList().execute();
                else
                    new SearchRockStarsList().execute(etSearch.getText().toString());
            }
        });
    }

    private void loadData() {
        new RequestRockStarsList().execute();
    }

    private class RequestRockStarsList extends AsyncTask<String, Void, List<MyRockstar>> {
        private ProgressDialog progress;

        @Override
        protected List<MyRockstar> doInBackground(String... params) {
            String json = Tools.getRequest(getString(R.string.url_server) + getString(R.string.methode_rockstars));
            if (json != null) {
                try {
                    return JsonParser.getRockStars(getActivity(), json);
                } catch (JSONException e) {
                    return null;
                }
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<MyRockstar> result) {
            progress.dismiss();
            swipeLayout.setRefreshing(false);
            if (result == null) {
                Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            } else {
                data = result;
                lvRockstars.setAdapter(new AdaptRockstars(getActivity(), result));
            }
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(getActivity(), "",
                    getString(R.string.label_loading), true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }

    private class SearchRockStarsList extends AsyncTask<String, Void, List<MyRockstar>> {
        private ProgressDialog progress;

        @Override
        protected List<MyRockstar> doInBackground(String... params) {
            String key = params[0].toLowerCase();
            List<MyRockstar> rockstars = new ArrayList<>();
            for (MyRockstar row : data) {
                if (row.getFirstname().toLowerCase().contains(key) || row.getLastname().toLowerCase().contains(key))
                    rockstars.add(row);
            }
            return rockstars;
        }

        @Override
        protected void onPostExecute(List<MyRockstar> result) {
            progress.dismiss();
            swipeLayout.setRefreshing(false);
            if (result == null) {
                Toast.makeText(getActivity(), getString(R.string.msg_no_rock_star_found), Toast.LENGTH_SHORT).show();
            } else {
                lvRockstars.setAdapter(new AdaptRockstars(getActivity(), result));
            }
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(getActivity(), "",
                    getString(R.string.label_loading), true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
