package com.appstud.testenanotech.fragments;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.appstud.testenanotech.R;
import com.appstud.testenanotech.adapters.AdaptBookmark;
import com.appstud.testenanotech.entities.MyRockstar;
import com.appstud.testenanotech.tools.JsonParser;
import com.appstud.testenanotech.tools.Tools;

import org.json.JSONException;

import java.util.List;

public class FragBookmarks extends Fragment {
    private View view;
    private ListView lvRockstars;
    private List<MyRockstar> data;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.frag_bookmarks, container, false);
            loadControls();
            setListeners();
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadControls() {
        lvRockstars = (ListView) view.findViewById(R.id.lvRockstars);
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
    }

    private void setListeners() {
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new ReadLocalBookMark().execute();
            }
        });
    }

    private void loadData() {
        new ReadLocalBookMark().execute();
    }

    private class ReadLocalBookMark extends AsyncTask<String, Void, List<MyRockstar>> {
        private ProgressDialog progress;

        @Override
        protected List<MyRockstar> doInBackground(String... params) {
            String json = Tools.getData(getActivity(), getString(R.string.data_bookmarks));
            if (json != null || json.equals("")) {
                try {
                    return JsonParser.getRockStars(getActivity(), json);
                } catch (JSONException e) {
                    return null;
                }
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<MyRockstar> result) {
            progress.dismiss();
            swipeLayout.setRefreshing(false);
            if (result == null) {
                Toast.makeText(getActivity(), getString(R.string.msg_no_bookmark), Toast.LENGTH_SHORT).show();
            } else {
                data = result;
                lvRockstars.setAdapter(new AdaptBookmark(getActivity(), result));
            }
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(getActivity(), "",
                    getString(R.string.label_loading), true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
