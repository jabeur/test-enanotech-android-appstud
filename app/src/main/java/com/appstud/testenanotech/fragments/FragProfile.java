package com.appstud.testenanotech.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.appstud.testenanotech.R;
import com.appstud.testenanotech.tools.BitmapTransform;
import com.appstud.testenanotech.tools.Tools;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.IOException;

public class FragProfile extends Fragment {
    private View view;
    private Bitmap bProfile = null;
    private String imgPath;
    private ImageView ivProfile;
    private EditText etName;
    private Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            bProfile = bitmap;
            if (bProfile != null)
                ivProfile.setImageBitmap(bProfile);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.frag_profile, container, false);
            loadControls();
            setListeners();
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadControls() {
        ivProfile = (ImageView) view.findViewById(R.id.ivProfile);
        etName = (EditText) view.findViewById(R.id.etName);
    }

    private void setListeners() {
        getActivity().findViewById(R.id.bOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData();
            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadCameraIntent();
            }
        });
    }

    private void loadData() {
        String imgB64 = Tools.getData(getActivity(), getString(R.string.key_user_photo));
        if (!imgB64.equals("")) {
            bProfile = Tools.fromBase64(imgB64);
            ivProfile.setImageBitmap(bProfile);
        }

        String name = Tools.getData(getActivity(), getString(R.string.key_user_name));
        if (!name.equals("")) {
            etName.setText(name);
        }
    }

    private void saveData() {
        new SaveData().execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case 1:
                    Picasso.with(getActivity()).load("file://" + imgPath).transform(new BitmapTransform(500, 500)).resize(500, 500).centerInside().into(target);
                    break;
            }
        }
    }

    private void loadCameraIntent() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException ex) {

        }
        if (photoFile != null) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            cameraIntent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            startActivityForResult(cameraIntent, 1);
        }
    }

    private File createImageFile() throws IOException {
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                "tmp",
                ".jpg",
                storageDir
        );

        imgPath = "file:" + image.getAbsolutePath();
        return image;
    }

    private class SaveData extends AsyncTask<String, Void, String> {
        private ProgressDialog progress;

        @Override
        protected String doInBackground(String... params) {
            Tools.saveData(getActivity(), getString(R.string.key_user_photo), Tools.getImageBase64(bProfile));
            Tools.saveData(getActivity(), getString(R.string.key_user_name), etName.getText().toString());
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progress.dismiss();
            Toast.makeText(getActivity(), getString(R.string.msg_personal_info_saved), Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPreExecute() {
            progress = ProgressDialog.show(getActivity(), "",
                    getString(R.string.label_loading), true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
