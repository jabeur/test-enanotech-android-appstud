package com.appstud.testenanotech.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.appstud.testenanotech.fragments.FragBookmarks;
import com.appstud.testenanotech.fragments.FragProfile;
import com.appstud.testenanotech.fragments.FragRockstars;

public class AdaptDashPager extends FragmentPagerAdapter {
    public static final int FRAG_ROCKSTARS = 0;
    public static final int FRAG_BOOKMARKS = 1;
    public static final int FRAG_PROFILE = 2;
    private static final int PAGE_COUNT = 3;

    public AdaptDashPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int pos) {
        switch (pos) {
            case FRAG_ROCKSTARS:
                return new FragRockstars();
            case FRAG_BOOKMARKS:
                return new FragBookmarks();
            case FRAG_PROFILE:
                return new FragProfile();
            default:
                return new FragProfile();
        }
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
