package com.appstud.testenanotech.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstud.testenanotech.Dashboard;
import com.appstud.testenanotech.R;
import com.appstud.testenanotech.entities.MyRockstar;
import com.appstud.testenanotech.tools.JsonParser;
import com.appstud.testenanotech.tools.Tools;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class AdaptBookmark extends BaseAdapter {
    private Activity activity;
    private List<MyRockstar> data;

    public AdaptBookmark(Activity activity, List<MyRockstar> data) {
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int i) {
        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_rock_star_book_mark, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final MyRockstar rockstar = data.get(position);
        holder.tvName.setText(rockstar.getFirstname() + " " + rockstar.getLastname());
        holder.tvStatus.setText(rockstar.getStatus());
        Picasso.with(activity).load(activity.getString(R.string.url_server) + rockstar.getHisface()).config(Bitmap.Config.RGB_565).resize(500, 500).onlyScaleDown().centerCrop().into(holder.ivPicture);

        holder.bTrash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.remove(position);
                notifyDataSetChanged();
                ((Dashboard) activity).adapter.notifyDataSetChanged();
                new RemoveBookmark().execute(new String[]{rockstar.getFirstname(), rockstar.getLastname(), rockstar.getStatus(), rockstar.getHisface()});
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        private TextView tvName, tvStatus;
        private ImageView ivPicture;
        private Button bTrash;

        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.tvName);
            tvStatus = (TextView) view.findViewById(R.id.tvStatus);
            ivPicture = (ImageView) view.findViewById(R.id.ivPicture);
            bTrash = (Button) view.findViewById(R.id.bTrash);
            view.setTag(this);
        }
    }

    private class RemoveBookmark extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {

            String json = Tools.getData(activity, activity.getString(R.string.data_bookmarks));
            try {
                //read local bookmarks
                List<MyRockstar> localRockstars = new ArrayList<>();
                if (!json.equals(""))
                    localRockstars = JsonParser.getRockStars(activity, json);
                List<MyRockstar> toSave = new ArrayList<>();
                boolean exist = false;
                //remove the current selection
                for (MyRockstar row : localRockstars) {
                    if (!row.getHisface().equals(params[3])) {
                        toSave.add(row);
                    }
                }
                //save local bookmark
                String jsonToSave = JsonParser.setRockStars(activity, toSave);
                Tools.saveData(activity, activity.getString(R.string.data_bookmarks), jsonToSave);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {

        }
    }
}
