package com.appstud.testenanotech.tools;

import android.content.Context;

import com.appstud.testenanotech.R;
import com.appstud.testenanotech.entities.MyRockstar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonParser {

    public static List<MyRockstar> getRockStars(Context context, String json) throws JSONException {
        List<MyRockstar> rockstars = new ArrayList<>();
        JSONObject object = new JSONObject(json);
        JSONArray array = object.getJSONArray(context.getString(R.string.key_contacts));
        for (int i = 0; i < array.length(); i++) {
            JSONObject row = array.getJSONObject(i);
            MyRockstar rockstar = new MyRockstar();
            rockstar.setFirstname(row.getString(context.getString(R.string.key_firstname)));
            rockstar.setLastname(row.getString(context.getString(R.string.key_lastname)));
            rockstar.setStatus(row.getString(context.getString(R.string.key_status)));
            rockstar.setHisface(row.getString(context.getString(R.string.key_hisface)));
            rockstars.add(rockstar);
        }
        return rockstars;
    }

    public static String setRockStars(Context context, List<MyRockstar> data) {
        JSONObject obj = new JSONObject();


        try {
            JSONArray array = new JSONArray();
            for (MyRockstar row : data) {
                JSONObject rockstar = new JSONObject();
                rockstar.put(context.getString(R.string.key_firstname), row.getFirstname());
                rockstar.put(context.getString(R.string.key_lastname), row.getLastname());
                rockstar.put(context.getString(R.string.key_status), row.getStatus());
                rockstar.put(context.getString(R.string.key_hisface), row.getHisface());
                array.put(rockstar);
            }
            obj.put(context.getString(R.string.key_contacts), array);
        } catch (JSONException e) {

        }

        return obj.toString();
    }
}
