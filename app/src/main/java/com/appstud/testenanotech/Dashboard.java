package com.appstud.testenanotech;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.appstud.testenanotech.adapters.AdaptDashPager;

public class Dashboard extends AppCompatActivity {
    public AdaptDashPager adapter;
    private ViewPager vpDashboard;
    private ImageView ivRockstars, ivBookmark, ivProfile;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        loadControls();
        setListeners();
    }

    private void loadControls() {
        tvTitle = (TextView) findViewById(R.id.tvTitle);

        ivRockstars = (ImageView) findViewById(R.id.ivRockstars);
        ivBookmark = (ImageView) findViewById(R.id.ivBookmark);
        ivProfile = (ImageView) findViewById(R.id.ivProfile);
        setIndicator(AdaptDashPager.FRAG_ROCKSTARS);

        vpDashboard = (ViewPager) findViewById(R.id.vpDashboard);
        adapter = new AdaptDashPager(getSupportFragmentManager());
        vpDashboard.setAdapter(adapter);
        vpDashboard.setOffscreenPageLimit(1);

    }

    private void setListeners() {
        findViewById(R.id.llList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vpDashboard.setCurrentItem(AdaptDashPager.FRAG_ROCKSTARS);
            }
        });

        findViewById(R.id.llBookmark).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vpDashboard.setCurrentItem(AdaptDashPager.FRAG_BOOKMARKS);
            }
        });

        findViewById(R.id.llProfile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                vpDashboard.setCurrentItem(AdaptDashPager.FRAG_PROFILE);

            }
        });

        vpDashboard.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setIndicator(int selection) {
        //set the indicator of current page, indicate title, images and allowed button on shared action bar
        switch (selection) {
            case AdaptDashPager.FRAG_ROCKSTARS:
                findViewById(R.id.bOk).setVisibility(View.INVISIBLE);
                tvTitle.setText(getString(R.string.label_rockstars));
                ivRockstars.setImageAlpha(255);
                ivBookmark.setImageAlpha(127);
                ivProfile.setImageAlpha(127);
                break;
            case AdaptDashPager.FRAG_BOOKMARKS:
                findViewById(R.id.bOk).setVisibility(View.INVISIBLE);
                tvTitle.setText(getString(R.string.label_bookmark));
                ivRockstars.setImageAlpha(127);
                ivBookmark.setImageAlpha(255);
                ivProfile.setImageAlpha(127);
                break;
            case AdaptDashPager.FRAG_PROFILE:
                findViewById(R.id.bOk).setVisibility(View.VISIBLE);
                tvTitle.setText(getString(R.string.label_profile));
                ivRockstars.setImageAlpha(127);
                ivBookmark.setImageAlpha(127);
                ivProfile.setImageAlpha(255);
                break;
        }
    }
}
