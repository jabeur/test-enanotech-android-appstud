package com.appstud.testenanotech.entities;

public class MyRockstar {
    private String firstname;
    private String lastname;
    private String status;
    private String hisface;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHisface() {
        return hisface;
    }

    public void setHisface(String hisface) {
        this.hisface = hisface;
    }
}
